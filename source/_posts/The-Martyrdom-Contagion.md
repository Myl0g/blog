---
title: The Martyrdom Contagion
date: 2023-11-26 20:28:54
tags:
---

The rhetoric we have been seeing regarding the שמחת תורה (Simchat Torah) war about "ceasefire," "genocide," "apartheid" and the rest did not start when Israel invaded Gaza.

It started when Gaza invaded Israel.

The overwhelming popular response to the death of 1,200+ Jews was to celebrate our deaths, and then quickly pivot to condemning us for either existing or for responding to the attack. Sometimes [they did not wait](https://www.msn.com/en-us/news/world/cornell-university-professor-calls-hamas-terror-attack-exhilarating-and-exciting/ar-AA1ijrRm) before performing this second part, combining celebration and protest into one.

Yes, the same people protesting the humanitarian crisis in Gaza today --- and ascribing blame for it to the Jews --- celebrated our deaths. Human rights for thee, not for me.

This made it even more baffling to me when former allies started joining the crew in droves. Don't they see who they are aligning themselves with?

Maybe they do. But they also see the dead bodies of the forced martyrs in Gaza.

Yet this is not deception and these people are not deceived. It is 2023, where the only thing stopping our former friends from learning about Hamas's human shielding tactics, from learning basic international law, from learning the definition of genocide --- is a few button clicks.

No, these people are not deceived. They are choosing to know lies over truth, fictions over facts. Because only the fictions can give our new killers their license to hunt.

So the new --- no, the old --- story goes, "it is good to kill Jews because they are killing Gazans." Or, better yet, "the October 7th pogrom was justified because of the oppression of Gazans." Nevermind our [disengagement](https://web.archive.org/web/20231122083259/https://www.npr.org/2023/11/21/1214529460/former-israeli-prime-minister-reflects-on-the-2005-withdrawal-from-gaza) and evacuation from the Strip almost a decade ago. Nevermind that the blockade was [only imposed after a hostile takeover](https://web.archive.org/web/20231125172618/https://www.britannica.com/place/Gaza-Strip/Blockade) by a terrorist organization. Nevermind that Egypt enforced the blockade just as much as we did. No, those facts must be disregarded, lest they get in the way of killing Jews. Fiction provides the grounding for a new moral code. Except it is not new. It is old, very old.

"We need a ceasefire," they say, "to provide aid to the civilians." The Jew knows they mean aid to Hamas, [because that is where all aid goes](https://web.archive.org/web/20231024170720/https://www.jns.org/unrwa-fuel-medical-equipment-taken/). The Jew knows they mean to allow more time for Hamas to [stuff](https://web.archive.org/web/20231125235923/https://www.washingtonpost.com/opinions/2023/11/14/hamas-human-shields-tactic/) more children into [munitions facilities](https://web.archive.org/web/20230208003106/https://thehill.com/opinion/international/557939-how-congress-can-fight-hamas-use-of-human-shields/), [in command cells operating from hospitals](https://web.archive.org/web/20231126211349/https://www.theguardian.com/world/2023/oct/30/human-shield-israel-claim-hamas-command-centre-under-hospital-palestinian-civilian-gaza-city), because then the martyrdom contagion can spread further. The Jew knows it --- and the protestor knows it, too. They have it right at their fingertips. How could they _not_ know it? What is more absurd? The suggestion that these pro-Palestinian (more accurately, pro-Hamas) supporters have no idea they are spewing libels against Jews, even though one Google Search confirms it? Or the idea that they _do_ know it --- and don't care?

When was the last time any group chanted "Itbah al-Yahud?" When was the last time any group chanted "Death to Jews?" The new brownshirts do something much more effective: they call for other things that _will_ kill Jews instead of asking for it directly. They cloak their death-talk in "social justice" or "human rights" language to legitimize the feelings of the outsider looking in: the closet antisemite yearning for the chance to be free. They spread their martyrdom contagion far and wide.

This contagion is a virus like no other. It is the only one where you must choose to be infected. It is an entirely voluntary procedure, undertaken by people who otherwise exercise basic critical thinking and research tactics --- just not for Jews. We get nothing but regurgitated blood libel.

What did the old brownshirts protest? Jewish existence? Only in their hearts. To the rest of the world, they protested [economic conditions, political dysfunction, Communism, and more](https://web.archive.org/web/20220808194012/https://las.illinois.edu/news/2014-10-01/how-did-nazis-become-so-popular). The German voter knew it was all a cover for the true agenda --- Hitler had already published _Mein Kampf_, explicitly describing himself as an annihilationist antisemite! --- yet they voted for the party anyway. Why? Haven't I told you why?

Hitler's journey, his _Kampf_, began with tolerance and good naturedness to the Jewish people, becoming antisemitic over time, eventually deciding on genocide as the final solution. Today's new guard of brownshirts are individually retracing his steps.

What do the new brownshirts protest? Humanitarian conditions, perceived political dysfunction, Zionism, and more. They don't need to say "Itbah al-Yahud" when they can say "from the River to the Sea." We fill in the blanks.

The natural question is: is their variety of Jew killing brand new, or is it old? [Ask them](https://web.archive.org/web/20230311115146/https://www.algemeiner.com/2017/05/14/the-palestinian-connection-to-the-nazis/): [they will tell you](https://web.archive.org/web/20230207202203/https://blogs.timesofisrael.com/%E2%80%8Bthe-nazi-roots-of-palestinian-nationalism/). It is not a coincidence that [the new volk parrot the talking points of "neo"-Nazis](https://docs.house.gov/meetings/JU/JU00/20231108/116550/HHRG-118-JU00-20231108-SD011.pdf); not a coincidence that when the Nazis come to their rallies and marches, they are [disavowed after the fact and not during](https://web.archive.org/web/20231025224620/https://politicalresearch.org/2023/10/20/advisory-no-nazis-our-streets), because their speech and rhetoric is identical as to be completely indistinguishable from their own.

The American education system has failed. All of the Holocaust education was worth nothing. All of the anti-bigotry and multicultural awareness was meaningless. The whole point was to ask the young people, "if you were there, what would you have done?" and for them to answer "I'd defend the Jews!" like good students. A+ on your "No Jew Hating" quiz. Years later now, they were given the same quiz again.

Their new --- no, their old, their _ancient_ --- answers?

#### "End Zionist Apartheid!"

### "Khaybar, Khaybar, oh Jews!"

## "Ceasefire Now! End Zionist Genocide!"

# "Itbah al-Yahud!"

# Death to the Jews! 

We gave the education system the gravest task in its history --- end the oldest hate in the world --- and it failed. Students from my high school, having the greatest Holocaust education I have ever seen, have joined the legion of brownshirts. One of them reposted [Norman Finklestein, the Holocaust denier](https://blogs.timesofisrael.com/did-norman-finkelstein-just-deny-the-holocaust/). Should I be surprised?

The youngest Americans, [in recent polling](https://harvardharrispoll.com/wp-content/uploads/2023/11/HHP_Nov23_KeyResults.pdf), are near-evenly divided on whether to support Hamas more than Israel. Should I be surprised? The oldest hate now dominates this country, and I have never felt this close to my father's father, living in a Polish village as the jaws of his people's executioner drew ever nearer, silently, as his fellow _Yidden_  echoed the Nazi siren song, telling him he had nothing to fear. Should I be surprised?

I am not. I am not surprised that the same story we have been told for 3000 years is being told again. I am not surprised that former friends have joined the brownshirts to courageously lead the charge against my existence, from the comfort and safety of euphemisms and doublespeak. I am not surprised to be just as disposable now as I was in 1939.

The only ones who will be surprised are the ones who thought their assimilation would save them.

---

"And then, suddenly, one day in 1939, it all ceased to be." - My grandfather Aryeh
