---
title: How can UCF fix its outmatched administration? Make the colleges do it instead
date: 2021-08-17 12:48:10
tags:
- University of Central Florida
- College
- Academics
- Logistics
---

The modern American university consists of a central bureaucracy overseeing its academic colleges, with administrative duties split up between the two. At UCF, the central office handles academic recordkeeping, financial aid, and admissions, while the colleges handle degree certification and academic advising (among other tasks I may be unaware of).

That system has been put under strain over the years, as state and federal budget restrictions make it impractical for the University to devote its increasingly-limited hiring powers to administrative duties as opposed to academic faculty. As enrollment grows, administrative capacity has not kept pace, resulting in situations where students may need to wait weeks for a response to an important email, or send in 8 separate copies of their college transcripts (at $8 per transcript, I think the University ought to reimburse me with a $64 scholarship). To remedy this, UCF subcontracted most (if not all) of its financial aid and undergraduate admissions (representing the vast majority of the University's administrative duties) to third parties starting in Summer 2021. This is great for the average student, seeking admission in their last year of high school and requiring no special consideration. For everyone else, it's a complete disaster, as communication between these subcontracted call centers and the skeleton crew comprising the "upper-level" of the admissions and financial aid departments is practically non-existent. The University, desiring to retain some level of decision-making autonomy in these departments, tightly restricts the authority of the call center staff, preventing them from making important decisions for students with special cases and from directly contacting managment on the UCF campus. A callback, which usually goes unhonored (I know from personal experience), is the best they can do.

Time-consuming bureaucracy is a part of the American lifestyle, but for students in desparate need of financial aid to pay their rent or attend class, it's entirely unacceptable.

The easiest, and possibly the worst, solution to this problem is to pump more money into Millican Hall\*. Not only would it involve spending more money on a function of the University which is tangential to its core educational mission, it would also likely result in a more convoluted and potentially corrupt upper managment. Some students will [remember the last time the University had more money than it knew what to do with](https://en.wikipedia.org/wiki/University_of_Central_Florida#Colbourn_Hall_scandal)**.

The ideal solution, in my world, would have the following qualities:

- Keeps pace with its clients in 65%+ of cases,
- Handles time-sensitive situations within the required timeframe in 100% of cases,
- Doesn't make students feel like they ought to call the Ombudsman just to get an answer to what's happening with their case, and
- Doesn't cost more than the current solution.

I want a bureaucracy that works efficiently, handles emergencies well, doesn't feel like a bureaucracy, and doesn't cost more than the current bureaucracy. A nice fairy tale, but with enough decentralization it might be made into a reality. At the start of this post I said that the central bureaucracy splits its duties roughly 60-40 with the colleges. What if this split more closely resembled 5-95, similar to an Oxford-style university? 

The only role I can imagine the central bureaucracy fulfilling better than the colleges is academic recordkeeping, which requires the participation of multiple colleges to compile a complete academic record. Everything else - admissions, financial aid, degree certification, academic advising, and even tuition/account payment - seems to be well suited for handling on a college-level. In all administrative operations, the colleges would have a fraction of the administrative burden compared to Millican Hall, which could give admin-related funds to the colleges proportionally based on their average enrollment and the size of their application pools. They would be able to pay closer attention to each individual student, avoiding the salient issues raised by automated systems and toothless call centers.

One minor wrench that could be thrown into the gears of this new system is the federal nature of student financial aid. Would the colleges be authorized to make transactions directly with the federal government, circumventing the University altogether? Probably not, but Millican Hall could retain an automated system that processes financial aid transactions submitted by the colleges, as well as a small number of staff to talk to the colleges' liasons in the event of a special case. This would add minor costs to that portion of the system but keep its overall efficiency more or less intact.

A more important issue: would this new system actually cost the same as the current one? In the short-term, I doubt it. The University would need to give extra funds to the colleges for one-time setup expenses: purchasing computer systems and initiating hiring processes for new staff. These costs aren't factored into the long-run performance, but keeping pace with current enrollment at an exponentially-growing university inevitably means spending more money. While decentralization can help the University achieve administrative efficiency, we can't expect it to achieve maximum efficiency in the absence of reasonable funding. If the University's enrollment expands from its current 70,000+ to 100,000 in the next 5-10 years, and the funds allocated to the colleges for administration remain the same, even this new system will fail miserably to serve its students.

Of course, the greatest solution of them all would replace UCF with several smaller Universities that can [actually seat their students](https://www.orlandosentinel.com/news/education/os-ucf-first-week-crowded-20150828-story.html), but considering that the very existence of UCF is a Florida law, I doubt this will come to pass. So I have to be a bit more pragmatic.

---

*A building at the southern end of the Orlando campus, containing the offices of all the constitutent departments of the University's central managment. The University of Maryland, College Park should take note of this model instead of spreading their administrative offices around campus like confetti, making it rather difficult to interact with the administration.

**To be fair to former President Hitt and his staff, Colbourn Hall remains one of the nicest buildings on campus to this day.
