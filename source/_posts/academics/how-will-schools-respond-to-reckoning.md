---
title: How will schools respond to the ChatGPT Reckoning?
date: 2023-06-18 13:10:10
tags:
- College
- Academics
- AI
---

I was disappointed that my school did not, as a matter of policy or unanimous agreement, abolish the take-home essay upon the release of ChatGPT. Leave it to a bureaucratic institution to be slow to respond to a situation that destroys a several hundred-year-old status quo. But my naive mind strongly believes they will respond to it, so I may as well speculate on how they will.

Before we begin, I think it's important to understand how ChatGPT has so much disruptive potential. ChatGPT doesn't prevent anyone from doing their assignments on their own. It just allows those who don't want to do their homework to successfully avoid it. Previously, you'd need someone else to do it for you, which meant paying them and getting something of questionable quality. The cost of doing it yourself used to be less than the cost of someone else doing it. Now there's not even a comparison. Your homework can be done in less than 5 seconds at the exact same quality you'd do it yourself and at no financial cost. The only benefit of doing your own homework is learning, so if you don't view that as a substantial-enough benefit, you just won't do it.

The truth is that, thanks to a combination of factors, the vast majority of aspiring white-collar workers view college as necessary and attend not out of a desire to learn but out of a desire to obtain a piece of paper. If shortcuts are available to someone in that situation, they will take them. Computer science majors don't want to learn about philosophy and psychology (even though everyone should!), and the tools they have available to avoid doing do (sleeping in lecture, procrastination) now include the Get-out-of-Homework Free card. Not to mention the just as important factor that college coursework in technical subjects is deeply inferior in quality to professional certification and on-the-job training.

Many have meditated on the "Death of the Liberal Arts Education" although few I have seen connected it to the prevalence of technical majors in universities. Humanities students come in _wanting_ to learn so I can see why humanities professors are woefully unprepared to deal with students who _don't want_ to learn. But non-humanities students lack the intrinsic motivation to learn. The solution is either to spark the intrinsic motivation by getting students interested in psychology/philosophy/English/history or provide _worthwhile_ extrinsic motivation to make it feel less like a waste of time for them.

Of course, that's not what schools are going to do. It would be too effective. Here's what I think they'll actually do:

## Option 1: Everything in Class

In AP World History, we wrote our essays in class. Some of them were short-answer and some were document-based 3-page long treatises. But they were all done in class. This might be the easiest solution that schools/educators pursue in this post-chatapocalyptic world. If you can't guarantee your students actually write their essays outside of class, you can sure guarantee they do in class.

Any in-class approach that also allows technology usage would miss the point pretty heavily. Students have been using their phones to cheat in class for years. I can remember finishing my AP Computer Science exam and getting up from my desk only to see around half of my classmates checking their answers under their desks. ChatGPT has a pretty mobile-friendly site.

As far as false solutions (those which address symptoms and not their root causes) go, I like this one. It acknowledges that homework is useless in this day and age and means I don't have to do it. But while the big focus has been cheating on essays with ChatGPT, Chegg and Mathway have allowed students to cheat in math/physics classes for much longer than ChatGPT's been around for. The sheer importance of practice in these classes means I doubt they can get away with doing _everything_ in class. 

## Option 2: Document History

If I was a technically-savvy administrator who wanted to address the symptoms of this problem, I'd find a way around doing everything in class: track everything done out of class. 

The student information system would create something like a Google Doc for every assignment, and would automatically submit that assignment at the deadline. No other items would be permitted for submission. The advantage here is that Google Docs track history pretty well - if you copy-pasted from ChatGPT, it would know. If you typed out ChatGPT's output by hand, it could theoretically see you alt-tabbing way more than normal (yes, your browser does give that info away - which is most notably used by Canvas quizzes). 

But this would probably flag source-referencing and research as suspicious activity. Also, alt-tabbing is very easily defeated by another device being present - so unless we want to implement ProctorHub for Essays (which also wouldn't work for similar reasons) it'll be very easy to get around this requirement. There's also a certain absurdity to the idea of implementing document histories for every software which would be used for assignments - Photoshop? iMovie? Visual Studio? Vim?

## Option 3: Abolish the Essay!

Down with the graphiarchy! Essays can be cheated too easily? Get rid of them! _My_ ideal English class is a 3 hour Socratic seminar where _everyone_ is forced to participate and graded on... some arbitrary criteria! Those who disagree get hemlock!

Writing teaches very important skills: collecting your thoughts into a coherent argument, understanding how other people will perceive your words (so you know how to properly express your ideas, and maybe even use rhetoric to achieve a desired effect on your reader), and finding research to support your ideas. Eschewing the essay altogether will deprive your students even more than ChatGPT does, because at least your students see the essays it writes. Something is better than nothing.

Socratic seminars are valuable in their own right. They don't need to crowd out essays as well. It also doesn't apply to the sciences (try doing a Socratic seminar on Gauss's law).

(Side anecdote: in my 8th grade English class my classmates ran a fantasy Socratic Seminar league based on participation. I don't think my teacher was prepared for just how engaged we became after that. The talking-over became progressively more difficult to moderate until the league was officially banned after what amounted to a screaming match to guarantee a win. No money was involved; we were just bored middle schoolers.)

## How Long Will We Keep Doing This?

Knowing that technical majors in college are present not out of intrinsic motivation but extrinsic compulsion - if my internship programs didn't require college enrollment I'd be an idiot to attend school - makes all this seem rather absurd. Doesn't it spark a conversation in the administrative backrooms when more students cheat to pass than pass legitimately? Plenty of people have written about the concept of grade inflation, which stems from the same apathy towards education pushing universities to make things easier for the students who don't want to be there. Even more people have written about the financial inflation of tuition and its causes, which further reduces the value of a liberal arts education ("Why are we spending $20,000 each semester on a History degree when your annual salary will be about the same amount?") while breeding additional resentment amount the technical majors who can afford it... and consider it highway robbery.

It's not that something _has_ to change, it's that something _will_ and _is_ changing - just not the colleges. Increasing numbers of employers are dropping college-education requirements. Tesla never had it to begin with (they politely declined my application a semester before I began college). The morons stuck in the college-admin backrooms aren't going to change, and in that weakness there is wonderous opportunity for those willing to seek it. Traditional companies who partner with colleges to keep tech students trapped in the educational system in order to continually access their talent pools are going to get their asses kicked by companies who provide far more value to their employees by allowing them to press "Skip". Younger corporate cohorts replicate the college lifestyle without the deadweight homework assignments (or more accurately the deadweight GPT-4 subscription).

The longer this goes on for, the more expensive it is going to get for students to be given the opportunity to use ChatGPT. If companies don't right now realize this is ridiculous then they will once their smarter competitors undercut them. It will be a race to see how quickly corporations can abandon colleges and appeal directly to their new talent pool, no middleman University need apply. Hopefully then we can get back to the era of dirt-cheap liberal arts education. 

