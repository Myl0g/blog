---
title: UCF Finally Responded to ChatGPT
date: 2023-09-18 16:47:00
tags:
- Academics
- College
- Education
- AI
---

UCF's Vice President for "Student Success and Well-Being" recently pushed a campus wide announcement, shown below:

"What Students Should Know About ChatGPT and Other AI

Chat GPT and other artificial intelligence (AI) tools can be helpful resources for research, brainstorming, and assistance. However, as a student at UCF, it is important for you to understand expectations around how these tools should be used.

How does ChatGPT work?

ChatGPT and other AI chatbots are trained to conduct thorough searches of information available across the Internet and use that information to build statistically-probable responses to prompts. This results in a seemingly correct answer, but it is important to double-check chatbot “facts.” These systems can invent facts, names, quotes, titles, and more, presenting them with confidence as truth. Chatbots are word predictors, not verified sources of accurate information.

When should I use ChatGPT? When should I avoid it?

UCF does not have an established policy on ChatGPT. Some of your faculty may allow you to incorporate AI in your assignments, while others may ban it. It is your responsibility as a student to understand each of your faculty members’ policies. When in doubt, ask your professor, or play it safe and assume that use of AI is banned.

If use of AI is permitted, you should still never submit a ChatGPT or other AI platform’s words as if they are your own. Attempting to pass off work created by another source as your own is plagiarism, and plagiarism puts your academic future at risk.

How would plagiarizing using AI be caught?

Detection to recognize AI writing is built in to UCF's Turnitin tool directly within Webcourses. This detector produces an “AI score” the same way they get an "originality score."

Can I use ChatGPT for help with online exams?

No. Faculty use a lockdown browser (which does not allow new browser tabs) and an electronic proctoring solution with online exams. The proctoring software records you via webcam and records what is on your screen.

Knights, you are here at UCF to earn a degree that demonstrates your readiness to enter the workforce. Tools like ChatGPT and AI can assist you on that journey, but they are not shortcuts to reading, studying, and learning that will help you be prepared for life after college.

Should you choose to use these tools, proceed with caution, and understand the expectations around academic integrity.

Charge On!"

In the wonderful work of science fiction *Foundation* by Issac Asimov, a diplomat is called upon by the fledgling Foundation to discuss protection in the face of a threatening, nuclear power developing nearby. The leaders converse with the diplomat and record the statements he makes, and subsequently subject them to logical analysis by translating the recordings into a logical language. They find that at all points where the diplomat makes a statement, he later retracts or refutes that statement (we will supply you with an army... assuming, of course, that the correct permissions are secured from General So-and-so) such that the translation amounts to nothing - literally a blank piece of paper - because nothing was actually said.

Allow me to subject the veep's message to the same treatment:

"ChatGPT is very useful to students, **but** sometimes it lies. We will not be establishing a blanket policy on its use, **but** actually you should assume it is banned, even if nobody tells you outright that it's banned. You shouldn't submit ChatGPT's work as your own, **but** as an institution we won't prevent you from doing so. We allow Professors to use TurnItIn to check your work for AI influences, **but** we make no guarantees about the reliability of TurnItIn (and actually if you look at the research it is about as good at its job as snake oil is good at curing blindness). If you use ChatGPT in an online exam, and our proctoring software catches you, we consider it academic dishonesty,  **but** we're assuming our online exam monitoring tools will catch you in the first place."

Now let's apply some basic mathematics: cancel out every statement that has a "but" attached to it. We end up with:

"    "

Nothing. In this part of *Foundation*, the leaders of the Foundation become (rightly) terrified.

In that sense, it is not surprising that this veep took *9 months* to make a statement about the technology which poses the most credible threat thus far to the hundred-year-old status quo of higher-ed. To craft a statement which mentions the existence of such a threat, and proceeds to *say literally nothing about it*, requires a lot of work. 9 months is almost early!

(It is also not surprising from another perspective: this veep of "Student Success and Well-Being" presides over an institution which is so notorious for not graduating its students on time that we call it "U Can't Finish." I wonder how this veep measures student success... I imagine *not* by percentage of students who graduate on time!)

Thankfully for UCF, relatively few students (somewhere among 30-40%) have told me that they regularly use ChatGPT to assist their studies. Not so thankfully, when I suggest to the remaining 60-70% that they should use ChatGPT (never to cheat; I am not unethical!) they respond with some variation of "Ahh! Now that's a good idea!" So it seems that UCF's time sticking their heads into the sand is running low.

Besides this update, I have nothing more to add regarding the impact of ChatGPT on higher-ed besides what I already wrote: http://milogilad.com/blog/2023/06/18/academics/how-will-schools-respond-to-reckoning/

(You can verify the [digital signature of this blog post](https://milogilad.com/assets/ucf-ai-statement-sig.txt) at [this link](https://keybase.io/verify).)
