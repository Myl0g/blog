---
title: 'A Homelabbing Follow-Up: Iterative Progress'
date: 2024-07-06 14:41:11
tags:
- Software
- Development
---

In my [previous post](https://milogilad.com/blog/2024/06/12/development/homelabbing-report/), I briefly mentioned that I intended originally to set up a VPC, with one server acting as the public-facing "router" and the others hosting services. That didn't pan out due to service provider limitations, so I gave up on the idea entirely.

I shouldn't have. In fact, that idea - refined slightly - could have saved me *a lot of money*. So much so that when the idea for my current homelab iteration came to me, I immediately migrated everything over to the new architecture and (after carefully testing the new stack and backing up the original stack) tore down the old stuff.

What was that slight refinement? It's realizing what the term "virtual private network" actually means. What it actually means is that you can take two or more hosts, put them *anywhere in the world*, and as long as a single host can be designated the router (with a public facing IP address) then you've got yourself an abstracted private network.

Let's say I have two hosts, A and B, in two different houses, neither with public IP addresses or port forwardings; and one host C with a public IP address/port forwarding. If I connect hosts A and B to host C with WireGuard, then host A can talk to host B by going *through* host C - even though host B doesn't have a public IP address, it does have a private IP address in the *virtual* private network.

So despite the fact that I currently do not have the ability to port forward my Raspberry Pi 4 and grant it a public IP address (arguably unwise to begin with!), I can still access it anywhere in the world I want - the perfect homelabbing workaround.

There are two caveats to this:

1. You still need a public IP address. I got a (separate) VPS running on the cheapest plan with a hosting company. What it lacks in CPU, RAM, and storage, it makes up for with 1 TB of bandwidth. That's plenty for my use case, and the whole setup only runs me $5/month (compare that to my previous posts' cost estimate).
2. You need an extra setting in your WireGuard configs - `PersistentKeepalive` under the `[Peer]`. That's because if your devices don't have a public IP address or port forward, they're being NAT'd through to the Internet, and after a certain period of time the original port being used for the NAT'd device will change *if it's not in use*. The persistent keep-alive option keeps that port in use, so the connection won't be dropped by your ISP. (I set my `PersistentKeepalive = 15` but the recommended is 25. I haven't tested that far yet.)

With that, I now have an actual homelab setup that can move with me! As long as I can keep my Raspberry Pi hooked up to the Internet (and keep the ailing hard drive connected to it happy) I have all of my services whenever, wherever. The dark magic of networking delivers once again!

---

"WireGuard" and the "WireGuard" logo are registered trademarks of Jason A. Donenfeld.
