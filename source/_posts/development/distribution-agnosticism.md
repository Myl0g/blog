---
title: 'Distribution Agnosticism: Going Beyond a Separate /home Partition'
date: 2020-07-11 13:39:58
tags:
- Linux
- System Administration
- Software
---

For the past 8 or so years that I've been using Linux, I've never found it necessary to create separate partitions for `/home`, `/boot`, `/tmp` and whatever else is an "important" folder you can find in the filesystem's root directory. This was for two reasons:

1. Partitioning on drives accessed through proprietary drivers (which I had done for 7 of those 8 years) was a scarring experience that I did not wish to repeat unless absolutely necessary
2. 99% of my important data is on the cloud anyway

Now I understand that putting `/home` on its own partition is not only a fantastic idea but also the only true way to experience Linux. And now I'm going to help you understand the same.

## How I used to use `/home`

I was new to the scene, so I made the sensible decision to leave well enough alone. I had all my eggs (home/boot/tmp) in one basket. I figured that those supreme beings, the developers of the Ubuntu operating system, wouldn't have allowed me to leave everything as one partition if it wasn't the best option.

And for some cases it sure is the best option. Ephermeral servers, Raspberry Pis (or is it Pies?), and anything else that *doesn't need persistent data* also doesn't need a separate `/home` folder.

That was not my case, and it probably isn't the case of anybody who uses Linux as their primary desktop operating system. The data in your home folder contains every single user-specific configuration file. Mail accounts, browser cookies, keychain data - all the data you generated when you logged into your various online accounts. And it is very irritating to set those up every time my home folder data is deleted (either on accident or on purpose).

## How I use `/home` now

I have a LUKS encrypted partition, mounted on boot through `/etc/fstab` and `/etc/crypttab`, that contains the contents of my home folder. 

Here is every single benefit I've discovered by using this system:

1. I don't have to rely on eCryptFS, which is useful for encrypting non-separated `/home` directories at rest, but is also terrible at data recovery
2. I can multiboot different distributions on different drives - using Arch, Ubuntu, or whatever else I choose - while retaining access to my online accounts and not creating an enormous number of app-specific passwords (I call this "distribution agnosticism")
3. If I manage to destroy my root filesystem, my home partition (should be) completely unaffected

The only downside is that migrating to this system takes a long time, depending on how much data is in your `/home` partition. Thankfully, the accidental destruction of my root filesystem is what motivated me to pursue this option, and as a result I did not have any data to migrate; but if you do prepare for a long and tedious copying operation.

## My model of Distribution Agnosticism

My newfound ability to multiboot distributions got me thinking. If I can simply swap out my current distribution for another one, barely sacrificing anything in the process, does that give me the opportunity to take Linux beyond the concept of the distribution?

Prior to this discovery of mine I perceived the distribution as paramount. It held the ultimate power over how I used Linux, how I percieved Linux, and how I interacted with software. Now that I've expanded beyond the concept of the distribution, I am made to challenge my perception of the Linux ecosystem, and how I should continue to use it.

My model of distribution agnosticism involves having multiple distribution installations, each for a specific purpose. I can use Elementary for schoolwork, Arch Linux for development, Linux from Scratch for system experimentation, and Quebes OS for accessing sensitive data. This model addresses a great concern of mine whenever I use Linux - the ease of re-tooling a system.

If I switch desktops from GNOME to i3 in Arch Linux, I can go about uninstalling the `gnome` group and installing the `i3` group. But wait: uninstalling the `gnome` group also uninstalls `gnome-keyring`, `eog`, and so much other stuff that I use regardless of the desktop manager at hand. Turns out these utilities are either (1) members of the `gnome` package group or (2) installed as direct dependencies of packages within the `gnome` group, without having been explicitly installed. And they aren't included in the `i3` group (which is intentionally minimalist). So now I am confronted with the enourmous task of manually removing all GNOME-related packages that I don't want, and keeping those that I do want. Simply knowing the difference between the packages in these groups is an immense task, and making the decisions of what I do and don't want has taken me hours of time that I didn't ask to spend.

Yes, I could suck it up and just install `i3` without removing `gnome`, but that is inefficient. Why keep unused software? It might even expose me to security vulnerabilites (after all, the more software I have, the more "surface area" a potential attacker has to find a way in).

Maybe the solution isn't to uninstall `gnome` and install `i3` - maybe the solution isn't to install `i3` at all. Maybe the solution is to create a completely separate instance of the distribution and install `i3` on that, and address issues with the setup as they present themselves (such as the initial absence of `gnome-keyring`). Making this process *totally independent of the existing software* ensures that I have a fallback in case `i3` is utterly incompatible with my way of life.

The development of several highly specialized and independent instances of Linux distributions creates redundancy - breaking changes made to one system have no impact on the others. The extreme separation of these systems can lead to a deeply secure setup, so long as proper sandboxing measures are taken (i.e. why should a distro geared to schoolwork have access to your personal documents?). It also means you don't have to worry about the aforementioned GNOME effect. The problems that arise from removing software that has deeply enmeshed itself with your system simply no longer exist.

## The feasibility of my model of Distribution Agnosticism

I can see this model being implemented with relative ease.

Let's assume you don't have a large external storage device, and that your hard drive is a standard 256 gigabytes in capacity. Major distributions - think Ubuntu, Fedora, and the like - don't take up more than 15 gigabytes with the base system + basic packages (i.e. Chromium, LibreOffice, all the other basic utilities) installed. So, assuming that you want three separate distros, and each will only consume (at most) 20gb, then you need only devote 60gb to these systems three. Your home folder can take up the remaining space (the boot partition is negligible). That sounds pretty simple to me.

The same is possible with external storage. A 64gb SD card will do the trick for the scenario I described above. There will be a (minor, most of the time) performance hit stemming from the fact that the software you use will be loaded from the external storage, but the benefit is that if your external storage fails then your home folder is completely fine (assuming that you keep it on the internal drive, which I would recommend).

Multiboot poses an initial challenge but is easily overcome. Assuming you use vanilla `systemd-boot` and `bootctl`, you can easily create multiple menu options based on the UUIDs of the partitions involved. You could probably write a script that does it for you:

```bash
#! /usr/bin/env bash

if [[ $(id -u) != 0 ]]
then
    echo "Please run as super/root user"
    exit 1
fi

blkid | while read -r line
do
    split=( $line )
    name=""
    uuid=""
    for val in "${split[@]}"
    do
        if [[ "$val" == *"UUID"* ]]
        then
            uuid=$val
        fi

        if [[ "$val" == *"PARTLABEL"* ]]
        then
            name=$val
        fi
    done

    printf "title\t${name}\nlinux\t/vmlinuz-linux\ninitrd\t/initramfs-linux.img\noptions\troot=${uuid} rw" > "$name".conf
done
```

Please do not take the above script to be sacred; it is entirely untested and simply exists to prove that a script to automate this process could exist. (Note that it requires all partitions to be labeled and assumes all of them will make use of the same `vmlinuz-linux` and `initramfs-linux.img` files).

# Should you do it?

Yeah, if you have too much time on your hands (like me).

It should be seriously considered by the individual who plans to use Linux in the long term for "important" work. It'll probably save you from most of the pain and sysadmin headaches associated with dealing with Linux. It'll certainly save you from the mistakes I made when I was starting out.
