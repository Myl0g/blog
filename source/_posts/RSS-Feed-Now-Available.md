---
title: RSS Feed Now Available
date: 2021-06-24 10:05:46
tags:
---

This blog now has a functioning RSS (Atom) feed. You can access it by clicking on the RSS icon in the top-right corner, or by visiting https://www.milogilad.com/blog/atom.xml. I recommend NetNewsWire as an RSS reader.
