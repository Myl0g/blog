---
title: Trust in the Digital Age
date: 2023-08-17 12:46:08
tags:
- Security
- Social Media
- Identification
---

A friend of mine was recently doxxed, but not for just any ordinary purpose. His identity was surreptitiously used to post vile comments to an elected official's website, along with many other people's stolen identities. Subsequently, this official released the comments in full, including the stolen personal information of my friend.

While this was by no means the official's fault, and they were obviously upset at the contents being posted to their website, the subsequent attribution of these remarks to my friend has significantly derailed his life. He lost his job, and then found a Google search of his name now contains all of the comments attributed to him. He has entirely lost control of his online identity, which (in today's age) means he has lost control of his identity in the real world, too.

My friend managed to convince the elected official that these were not his remarks and he had been doxxed. (I believe he showed her proof that his LinkedIn account had been hacked, making it the source of the personal information included in the form.) I imagine this is the least likely outcome for anyone targeted in this way. Most people attacked by celebrities, either justly or unjustly, do not subsequently gain audience with them. 

These terrible effects aren't the elected official's fault. The guy who doxxed my friend could have just as easily posted the vile remarks elsewhere. And, for as long as the Internet has been around, trolls have been doing stuff exactly like this. The thing is, if this had happened 10 years ago, I doubt anyone would have believed my friend actually posted those comments. It's not like they were in character for him! At the very least, people would have asked questions first. What changed?

## Widespread Adoption of the Internet, Not Internet Culture

More people use the Internet now compared to 10 years ago for things beyond just web browsing. People have very widely adopted social media, as well as other forms of dynamic content generation. We have increased smartphone usage and even normalized it in situations where it was normally taboo. (Think of schools which adopted the use of Kahoot, companies which issue work smartphones, etc.) In a sense it feels like we've succumbed to our addiction of content consumption. 

The problem is that the Internet is now increasingly integrated with the real world, without the vast majority of its users being "in" on Internet Culture. The inaccessibility of the Internet back then created a distinct set of norms and expectations among the people who actively engaged with it. It was never "OK" to impersonate someone to say terrible things, but it was something I would have expected from the Internet back then, and I would never have taken these statements at face value.

Most people who use the Internet now are lacking the mutual distrust which comes as part of Internet Culture. They take _everything_ at face value. Including absolutely outlandish statements made by someone with no reason to state them, all because some barely-personal info (name and email address) are included. If that's the standard for determining if someone really said something, then I could get every last person in my contacts list fired. Imagine how ridiculous it would be if banks allowed you to login using just your name and email address.

Anti-social behavior like this is so easily executed on the Internet that any mode of engaging with the Internet which doesn't account for it is fundamentally flawed.

## It Will Get Worse

The soon-to-be widespread use of deepfakes will make this problem worse. My friend was able to repudiate the statement attributed to him quite easily. If instead he had been deepfaked, would he be able to do the same?

There are telltale signs and AI detection tools which could potentially thwart a similar attack right now. Eventually, probably soon, these signs and tools will not work. Then, there will be nobody to vouch for the innocence of a deepfaked victim, and their exoneration (which is already quite rare) will not take place _at all_. 

The reputational damage and subsequent life-ruining potential of online impersonation does not result from the impersonation itself. It results from the widespread acceptance of the impersonation, which is itself a consequence of blindly trusting what we see. But we should know better than this. Most people's names and emails are publicly available. People's social media accounts get hacked all the time. The existence of deepfake technology is widely known. Why do we continue to fall for these traps?

## Solutions?

Security nuts (myself included) tend to sign our emails with PGP keys, and after seeing what happened to my friend, I considered extending that to all my written communication. But `gpg` is cumbersome and PGP keys are endlessly complicated. Subkeys and UIDs and signing vs. encrypting keys make the process rather unadoptable for people with better things to do. (That's even without considering the fact that `gpg` is a CLI and there are quite a few GUIs to choose from, which could put people off.)

The best alternative to getting everyone into PGP keysharing is [Keybase](https://keybase.io), which streamlines it for the modern Internet. They have a very easy-to-use [signature verification tool](https://keybase.io/verify) which allows you to validate easily. Although their cool and less-bulky Saltpack format can't be validated online, you can still use it for PGP signatures. And they make it pretty easy to generate PGP keys and signatures.

Are we all going to make Keybase accounts and start signing all of our online correspondence? Of course not. But if you'd like to protect yourself while waiting for people to realize that not everything online is real, you might consider it.

(You can verify the [digital signature of this message](https://milogilad.com/assets/trust-in-the-digital-age-sig.txt) at [this link](https://keybase.io/verify).)

---

Update on 9/18/2023: My friend's story has gotten even wider reach, thankfully. He was featured on his local NBC affiliate: https://www.nbcmiami.com/news/local/i-felt-my-future-was-gone-a-college-students-fight-to-restore-his-reputation/3113321/
